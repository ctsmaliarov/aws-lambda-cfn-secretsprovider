import os
import logging
import cfn_secret_provider
import cfn_rsakey_provider
import cfn_keypair_provider
import cfn_iam_publickey_provider
import cfn_accesskey_provider
import cfn_dsakey_provider
import cfn_secrets_manager_secret_provider

def configure_logger(loglevel=logging.INFO, logfile=None):
    logger = logging.getLogger()
    logger.setLevel(loglevel)
    format = "[%(filename)30s:%(funcName)20s():%(lineno)4s:%(levelname)-7s] %(message)s"
    if logfile and logfile is not None:
        logging.basicConfig(level=loglevel,
                            filename=logfile,
                            filemode='w',
                            format=format)
    else:
        logging.basicConfig(level=loglevel,
                            format=format)
    logging.getLogger('boto3').setLevel(logging.WARNING)
    logging.getLogger('botocore').setLevel(logging.WARNING)

configure_logger(logging.DEBUG) # TEST, loglevel should probably be an os.getenv("LOG_LEVEL", "INFO")
logging.debug("Logger configured")

def lambda_handler(request, context):

    if request['ResourceType'] == 'Custom::RSAKey':
        return cfn_rsakey_provider.handler(request, context)
    if request['ResourceType'] == 'Custom::DSAKey':
        return cfn_dsakey_provider.handler(request, context)
    elif request['ResourceType'] == 'Custom::KeyPair':
        return cfn_keypair_provider.handler(request, context)
    elif request['ResourceType'] == 'Custom::IAMSSHPublicKey':
        return cfn_iam_publickey_provider.handler(request, context)
    elif request['ResourceType'] == 'Custom::AccessKey':
        return cfn_accesskey_provider.handler(request, context)
    elif request['ResourceType'] == 'Custom::SecretsManagerSecret':
        return cfn_secrets_manager_secret_provider.handler(request, context)
    else:
        return cfn_secret_provider.handler(request, context)
